# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 38ms
    [gap of 11ms]
  create-initial-cxx-model completed in 98ms
create_cxx_tasks completed in 98ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 49ms
    create-module-model completed in 55ms
    create-module-model
      create-cmake-model 53ms
    create-module-model completed in 57ms
    [gap of 13ms]
  create-initial-cxx-model completed in 140ms
create_cxx_tasks completed in 141ms

