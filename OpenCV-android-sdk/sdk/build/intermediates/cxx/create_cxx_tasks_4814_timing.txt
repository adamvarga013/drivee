# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 40ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 38ms
    create-module-model completed in 42ms
    [gap of 11ms]
  create-initial-cxx-model completed in 108ms
create_cxx_tasks completed in 109ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 40ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 41ms
    create-module-model completed in 45ms
    [gap of 14ms]
  create-initial-cxx-model completed in 118ms
create_cxx_tasks completed in 119ms

