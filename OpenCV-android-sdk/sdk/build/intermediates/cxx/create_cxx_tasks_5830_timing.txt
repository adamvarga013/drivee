# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 43ms
    [gap of 11ms]
  create-initial-cxx-model completed in 107ms
create_cxx_tasks completed in 108ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 48ms
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 49ms
    [gap of 14ms]
  create-initial-cxx-model completed in 125ms
create_cxx_tasks completed in 126ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 47ms
    create-module-model completed in 52ms
    create-module-model
      create-cmake-model 47ms
    create-module-model completed in 50ms
    [gap of 12ms]
  create-initial-cxx-model completed in 125ms
create_cxx_tasks completed in 126ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 41ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 46ms
    [gap of 14ms]
  create-initial-cxx-model completed in 119ms
create_cxx_tasks completed in 121ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 42ms
    create-module-model
      create-cmake-model 40ms
    create-module-model completed in 44ms
    [gap of 12ms]
  create-initial-cxx-model completed in 111ms
create_cxx_tasks completed in 111ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 40ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 83ms
    create-module-model completed in 87ms
    [gap of 14ms]
  create-initial-cxx-model completed in 162ms
create_cxx_tasks completed in 163ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 41ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 19ms]
  create-initial-cxx-model completed in 111ms
create_cxx_tasks completed in 112ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 45ms
    create-module-model completed in 49ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 14ms]
  create-initial-cxx-model completed in 112ms
create_cxx_tasks completed in 113ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 42ms
    create-module-model
      create-cmake-model 38ms
    create-module-model completed in 41ms
    [gap of 11ms]
  create-initial-cxx-model completed in 104ms
create_cxx_tasks completed in 104ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 45ms
    create-module-model completed in 49ms
    create-module-model
      create-cmake-model 54ms
    create-module-model completed in 58ms
    [gap of 15ms]
  create-initial-cxx-model completed in 135ms
create_cxx_tasks completed in 136ms

