# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 38ms
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 38ms
  create-initial-cxx-model completed in 95ms
create_cxx_tasks completed in 96ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 66ms
    create-module-model completed in 70ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 44ms
    [gap of 11ms]
  create-initial-cxx-model completed in 138ms
create_cxx_tasks completed in 139ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 41ms
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 35ms
    [gap of 11ms]
  create-initial-cxx-model completed in 98ms
create_cxx_tasks completed in 99ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 49ms
    create-module-model completed in 53ms
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 48ms
    [gap of 17ms]
  create-initial-cxx-model completed in 133ms
create_cxx_tasks completed in 134ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 39ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 12ms]
  create-initial-cxx-model completed in 102ms
create_cxx_tasks completed in 103ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 47ms
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 46ms
    [gap of 11ms]
  create-initial-cxx-model completed in 118ms
create_cxx_tasks completed in 119ms

