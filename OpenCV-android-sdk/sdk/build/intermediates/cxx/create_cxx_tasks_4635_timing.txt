# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 37ms
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 37ms
  create-initial-cxx-model completed in 93ms
create_cxx_tasks completed in 94ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 40ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 43ms
    [gap of 12ms]
  create-initial-cxx-model completed in 110ms
create_cxx_tasks completed in 111ms

