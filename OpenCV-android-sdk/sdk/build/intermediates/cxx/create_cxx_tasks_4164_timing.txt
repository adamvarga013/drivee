# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 46ms
    create-module-model
      create-cmake-model 69ms
    create-module-model completed in 76ms
    [gap of 16ms]
  create-initial-cxx-model completed in 154ms
create_cxx_tasks completed in 155ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 47ms
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 48ms
    create-X86-model 17ms
  create-initial-cxx-model completed in 138ms
create_cxx_tasks completed in 139ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 39ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 11ms]
  create-initial-cxx-model completed in 101ms
create_cxx_tasks completed in 102ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 47ms
    create-module-model
      create-cmake-model 65ms
    create-module-model completed in 70ms
    [gap of 11ms]
  create-initial-cxx-model completed in 142ms
create_cxx_tasks completed in 143ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 42ms
    [gap of 10ms]
  create-initial-cxx-model completed in 106ms
create_cxx_tasks completed in 107ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 46ms
    create-module-model completed in 50ms
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 49ms
    [gap of 11ms]
  create-initial-cxx-model completed in 124ms
create_cxx_tasks completed in 125ms

