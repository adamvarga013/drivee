# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 38ms
    create-module-model completed in 41ms
    create-module-model
      create-cmake-model 42ms
    create-module-model completed in 46ms
    [gap of 24ms]
  create-initial-cxx-model completed in 126ms
create_cxx_tasks completed in 128ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 39ms
    [gap of 13ms]
  create-initial-cxx-model completed in 107ms
create_cxx_tasks completed in 108ms

