# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 32ms
    create-module-model completed in 35ms
    create-module-model
      create-cmake-model 32ms
    create-module-model completed in 35ms
    [gap of 14ms]
  create-initial-cxx-model completed in 96ms
create_cxx_tasks completed in 97ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 40ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 64ms
    create-module-model completed in 68ms
    [gap of 16ms]
  create-initial-cxx-model completed in 138ms
create_cxx_tasks completed in 140ms

