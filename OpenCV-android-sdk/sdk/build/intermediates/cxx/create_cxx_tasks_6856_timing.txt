# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 39ms
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 36ms
  create-initial-cxx-model completed in 94ms
create_cxx_tasks completed in 95ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 37ms
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 36ms
  create-initial-cxx-model completed in 91ms
create_cxx_tasks completed in 92ms

