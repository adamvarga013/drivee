# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 37ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 43ms
    [gap of 16ms]
  create-initial-cxx-model completed in 104ms
create_cxx_tasks completed in 105ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 48ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 10ms]
  create-initial-cxx-model completed in 107ms
create_cxx_tasks completed in 108ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 36ms
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 36ms
  create-initial-cxx-model completed in 90ms
create_cxx_tasks completed in 91ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 42ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 10ms]
  create-initial-cxx-model completed in 102ms
create_cxx_tasks completed in 103ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 39ms
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 38ms
    [gap of 10ms]
  create-initial-cxx-model completed in 96ms
create_cxx_tasks completed in 96ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 41ms
    create-module-model completed in 45ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 43ms
    [gap of 13ms]
  create-initial-cxx-model completed in 111ms
create_cxx_tasks completed in 112ms

