# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 37ms
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 38ms
    [gap of 10ms]
  create-initial-cxx-model completed in 96ms
create_cxx_tasks completed in 97ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 34ms
    create-module-model completed in 36ms
    create-module-model
      create-cmake-model 33ms
    create-module-model completed in 37ms
  create-initial-cxx-model completed in 92ms
create_cxx_tasks completed in 93ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 57ms
    create-module-model completed in 60ms
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 48ms
    [gap of 14ms]
  create-initial-cxx-model completed in 137ms
create_cxx_tasks completed in 138ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 47ms
    create-module-model completed in 51ms
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 38ms
    [gap of 13ms]
  create-initial-cxx-model completed in 114ms
create_cxx_tasks completed in 115ms

