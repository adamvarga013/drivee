# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 44ms
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 39ms
    [gap of 12ms]
  create-initial-cxx-model completed in 107ms
create_cxx_tasks completed in 107ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 39ms
    [gap of 11ms]
    create-X86_64-model 10ms
    create-module-model
      [gap of 10ms]
      create-cmake-model 35ms
    create-module-model completed in 46ms
    [gap of 13ms]
  create-initial-cxx-model completed in 120ms
create_cxx_tasks completed in 121ms

