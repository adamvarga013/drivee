# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 42ms
    [gap of 15ms]
  create-initial-cxx-model completed in 109ms
create_cxx_tasks completed in 110ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 38ms
    [gap of 12ms]
  create-initial-cxx-model completed in 100ms
create_cxx_tasks completed in 101ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 50ms
    create-module-model
      create-cmake-model 46ms
    create-module-model completed in 50ms
    [gap of 16ms]
  create-initial-cxx-model completed in 132ms
create_cxx_tasks completed in 133ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 50ms
    create-module-model completed in 54ms
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 47ms
    [gap of 15ms]
  create-initial-cxx-model completed in 130ms
create_cxx_tasks completed in 132ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 41ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 40ms
    [gap of 12ms]
  create-initial-cxx-model completed in 104ms
create_cxx_tasks completed in 105ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 68ms
    create-module-model completed in 72ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 42ms
    [gap of 16ms]
  create-initial-cxx-model completed in 143ms
create_cxx_tasks completed in 145ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 39ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 39ms
  create-initial-cxx-model completed in 98ms
create_cxx_tasks completed in 99ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 47ms
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 47ms
    [gap of 15ms]
  create-initial-cxx-model completed in 123ms
create_cxx_tasks completed in 124ms

