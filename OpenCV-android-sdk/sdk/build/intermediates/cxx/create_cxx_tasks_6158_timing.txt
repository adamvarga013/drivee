# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 39ms
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 37ms
    [gap of 24ms]
  create-initial-cxx-model completed in 117ms
create_cxx_tasks completed in 118ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 38ms
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 42ms
    [gap of 11ms]
  create-initial-cxx-model completed in 101ms
create_cxx_tasks completed in 102ms

