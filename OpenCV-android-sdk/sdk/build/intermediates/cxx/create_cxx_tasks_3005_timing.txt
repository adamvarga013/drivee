# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 35ms
    create-module-model completed in 44ms
    [gap of 11ms]
  create-initial-cxx-model completed in 115ms
create_cxx_tasks completed in 115ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 49ms
    create-module-model completed in 52ms
    create-module-model
      create-cmake-model 51ms
    create-module-model completed in 55ms
    [gap of 11ms]
  create-initial-cxx-model completed in 132ms
create_cxx_tasks completed in 133ms

