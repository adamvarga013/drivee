# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 38ms
    create-module-model completed in 41ms
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    [gap of 11ms]
  create-initial-cxx-model completed in 103ms
create_cxx_tasks completed in 104ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 51ms
    create-module-model completed in 56ms
    create-module-model
      create-cmake-model 44ms
    create-module-model completed in 49ms
    [gap of 11ms]
  create-initial-cxx-model completed in 133ms
create_cxx_tasks completed in 134ms

