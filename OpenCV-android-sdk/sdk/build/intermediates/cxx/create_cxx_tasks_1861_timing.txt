# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 42ms
    create-module-model completed in 46ms
    create-module-model
      create-cmake-model 43ms
    create-module-model completed in 47ms
    [gap of 14ms]
  create-initial-cxx-model completed in 121ms
create_cxx_tasks completed in 122ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 49ms
    create-module-model completed in 53ms
    create-module-model
      create-cmake-model 42ms
    create-module-model completed in 45ms
    [gap of 14ms]
  create-initial-cxx-model completed in 122ms
create_cxx_tasks completed in 123ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 40ms
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 39ms
    [gap of 11ms]
  create-initial-cxx-model completed in 100ms
create_cxx_tasks completed in 101ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 39ms
    create-module-model completed in 43ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 41ms
    [gap of 10ms]
  create-initial-cxx-model completed in 104ms
create_cxx_tasks completed in 105ms

