# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 41ms
    create-module-model
      create-cmake-model 37ms
    create-module-model completed in 39ms
    [gap of 11ms]
  create-initial-cxx-model completed in 103ms
create_cxx_tasks completed in 103ms

# C/C++ build system timings
create_cxx_tasks
  create-initial-cxx-model
    create-module-model
      create-cmake-model 38ms
    create-module-model completed in 43ms
    create-module-model
      create-cmake-model 36ms
    create-module-model completed in 39ms
    [gap of 12ms]
  create-initial-cxx-model completed in 103ms
create_cxx_tasks completed in 104ms

