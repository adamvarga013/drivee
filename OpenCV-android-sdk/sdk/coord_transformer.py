import sys
import re

#asift_file = sys.argv[1]
asift_file = "C:\\Users\\Adam\\source\\repos\\I0H4OC_ImageStitchingUsingHomography\\I0H4OC_ImageStitchingUsingHomography\\first_asift.txt"

cpp_matrix_header = '#Created by C++ matrix writer.'

coords = []
with open(asift_file) as fp:
    num_of_lines = fp.readline()
    counter = 0
    for line in fp:
        coordinates = line.split(' ')
        x1 = float(coordinates[0])
        y1 = float(coordinates[2])
        x2 = float(coordinates[4])
        y2 = float(coordinates[6].strip())
        coord = [x1, y1, x2, y2]
        coords.append(coord)
        counter += 1
        if counter == 50:
            break

print(coords)

with open("C:\\Users\\Adam\\source\\repos\\I0H4OC_ImageStitchingUsingHomography\\I0H4OC_ImageStitchingUsingHomography\\first_asift_converted.txt", 'w') as f:
    f.write(cpp_matrix_header)
    f.write ("\n")
    for coord in coords:
        f.write(str(coord[0]))
        f.write(" ")
    f.write("\n")

    for coord in coords:
        f.write(str(coord[1]))
        f.write(" ")
    f.write("\n")

    for coord in coords:
        f.write(str(coord[2]))
        f.write(" ")
    f.write("\n")

    for coord in coords:
        f.write(str(coord[3]))
        f.write(" ")

    f.write("\n")