package com.example.drivee

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import com.example.drivee.ui.home.HomeFragment
import com.example.drivee.ui.map.MapBoxFragment
import com.example.drivee.ui.settings.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.task.vision.detector.ObjectDetector

import org.tensorflow.lite.task.vision.classifier.ImageClassifier

import org.tensorflow.lite.task.core.BaseOptions

import org.tensorflow.lite.task.vision.classifier.ImageClassifier.ImageClassifierOptions
import java.io.FileOutputStream
import org.opencv.core.CvException

import org.opencv.imgproc.Imgproc
import org.tensorflow.lite.task.vision.detector.Detection
import java.io.File
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var cameraX: CameraX

    private var homeFragment = HomeFragment()
    private var mapBoxFragment = MapBoxFragment ()
    private var settingsFragment = SettingsFragment()


    private fun convertMatToBitMap(input: Mat): Bitmap? {
        var bmp: Bitmap? = null
        val rgb = Mat()
        Imgproc.cvtColor(input, rgb, Imgproc.COLOR_BGR2RGB)
        try {
            bmp = Bitmap.createBitmap(rgb.cols(), rgb.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(rgb, bmp)
        } catch (e: Exception) {
            Log.d("Exception", e.message!!)
        }
        return bmp
    }

    private fun writeImage (bitmap: Bitmap, i: Int, detection : Detection) {
        val h = bitmap.height
        val w = bitmap.width

        val bottom = if (detection.boundingBox.bottom < 0)  maxOf (0, detection.boundingBox.bottom.toInt()) else minOf (detection.boundingBox.bottom.toInt(), h)
        val top = if (detection.boundingBox.top < 0)  maxOf (0, detection.boundingBox.top.toInt()) else minOf (detection.boundingBox.top.toInt(), h)
        val left = if (detection.boundingBox.left < 0)  maxOf (0, detection.boundingBox.left.toInt()) else minOf (detection.boundingBox.left.toInt(), w)
        val right = if (detection.boundingBox.right < 0)  maxOf (0, detection.boundingBox.right.toInt()) else minOf (detection.boundingBox.right.toInt(), w)

        val y = top
        val x = left
        val height = (bottom) - y
        val width = (right) - x

        val bitmapCropped = Bitmap.createBitmap (bitmap, x, y, width, height)

        try {
            val fileOutputStream: FileOutputStream =
                this.openFileOutput("cropped$i", MODE_PRIVATE)
            bitmapCropped.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            fileOutputStream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun writeImages (bitmap : Bitmap, detections : List<Detection>) {
        for (i in 0..detections.size - 1) {
            writeImage(bitmap, i, detections[i])
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var fDelete = File ("/data/user/0/com.example.drivee/files/cropped0")
        fDelete.delete()

        fDelete = File ("/data/user/0/com.example.drivee/files/cropped1")
        fDelete.delete()

        fDelete = File ("/data/user/0/com.example.drivee/files/cropped2")
        fDelete.delete()

        fDelete = File ("/data/user/0/com.example.drivee/files/cropped3")
        fDelete.delete()

        fDelete = File ("/data/user/0/com.example.drivee/files/cropped4")
        fDelete.delete()

        // test opencv
        val initOpenCv = OpenCVLoader.initDebug()
        if (!initOpenCv) {
            print("opencd dead rip")
        }

        cameraX = CameraX(this.baseContext, this)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.background = null
        bottomNavigationView.menu.getItem(1).isEnabled = false
        if (savedInstanceState == null) {
            bottomNavigationView.selectedItemId = R.id.nav_home
            supportFragmentManager.beginTransaction().replace (R.id.main_layout, homeFragment).addToBackStack("home").commit()
        }

        // ----- NAV FRAGMENT -----
        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener {
            supportFragmentManager.beginTransaction().replace (R.id.main_layout, mapBoxFragment).addToBackStack("navi").commit()
        }

        // ----- HOME FRAGMENT -----
        val homeButton: View = findViewById(R.id.nav_home)
        homeButton.setOnClickListener {
            bottomNavigationView.selectedItemId = R.id.nav_home
            supportFragmentManager.beginTransaction().replace (R.id.main_layout, homeFragment).addToBackStack("home").commit()
        }

        // ----- SETTINGS FRAGMENT -----
        val settingsButton: View = findViewById(R.id.nav_settings)
        settingsButton.setOnClickListener {
            bottomNavigationView.selectedItemId = R.id.nav_settings
            supportFragmentManager.beginTransaction().replace (R.id.main_layout, settingsFragment).addToBackStack("home").commit()

        }

        cameraX.onCreate()

    }

    fun onNavigationStopped () {
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.selectedItemId = R.id.nav_home
        supportFragmentManager.beginTransaction().replace (R.id.main_layout, homeFragment).addToBackStack("home").commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CameraX.REQUEST_CODE_PERMISSIONS) {
            if (cameraX.allPermissionsGranted()) {
                cameraX.startCamera()
            } else {
                Toast.makeText(this,
                        "Permissions not granted by the user.",
                        Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

}