package com.example.drivee.ui.map


import android.app.AlertDialog
import android.app.AlertDialog.Builder
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.example.drivee.MainActivity
import com.example.drivee.R
import com.example.drivee.ui.utils.LocationPermissionHelper
import com.example.drivee.ui.utils.OfflineMapHandler
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.maps.*
import com.mapbox.maps.extension.style.expressions.generated.Expression
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.gestures.OnMapLongClickListener
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorBearingChangedListener
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import java.lang.ref.WeakReference
import androidx.core.app.ActivityCompat.startActivityForResult





class MapBoxFragment : Fragment() {

    private lateinit var mapView: MapView
    private lateinit var signHolder: LinearLayout
    private lateinit var mapboxMap: MapboxMap
    private lateinit var locationPermissionHelper: LocationPermissionHelper
    private lateinit var trafficSignAnalyzer: TrafficSignAnalyzer

    private var cameraTilt = 60.0

    fun getSignHolder() : LinearLayout {
        return signHolder
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)
        trafficSignAnalyzer = TrafficSignAnalyzer(this.context)

        mapView = view.findViewById(R.id.map) as MapView

        //signHolder = view.findViewById(R.id.signHolderLayout)

        val navBar: BottomAppBar = requireActivity().findViewById(R.id.bottomAppBar)
        navBar.visibility = View.INVISIBLE

        val fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.visibility = View.INVISIBLE

        val stopButton: AppCompatButton = view.findViewById(R.id.btn_stop)
        stopButton.setOnClickListener {
            (requireActivity() as MainActivity).onNavigationStopped ()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val handler = OfflineMapHandler(this.requireContext(), this.requireActivity())
        //If we're online, and the offline region is not available then prompt the user to download it
        if(!handler.offlineMapAvailable()) {
            if(handler.isOnline(this.requireContext()))
                showDialog(handler)
        }

        mapboxMap = mapView.getMapboxMap()

        locationPermissionHelper = LocationPermissionHelper(WeakReference(this.requireActivity()))
        locationPermissionHelper.checkPermissions {
            onMapReady()
        }
    }

    private fun showDialog(handler: OfflineMapHandler) {
        val b = Builder(this.requireContext())
        b.setTitle("Download offline maps")
        b.setCancelable(false)
        b.setMessage("It seems that you don't have the offline region downloaded yet. Would you like to download it now?")
        b.setPositiveButton("Yes" ) { _, _ -> handler.downloadOfflineRegion() }
        b.setNegativeButton("No", null)
        val dialog = b.create()
        dialog.show()

        if(dialog.window != null) {
            dialog.window!!.setGravity(Gravity.TOP);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLUE)
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLUE)
        }
    }

    private fun onMapReady() {
        mapView.getMapboxMap().setCamera(
            CameraOptions.Builder()
                .zoom(18.0)
                .pitch(cameraTilt)
                .build()
        )
        mapView.getMapboxMap().loadStyleUri(
            Style.MAPBOX_STREETS
        ) {
            initLocationComponent()
            setupGesturesListener()
        }
    }

    private fun setupGesturesListener() {
        mapView.gestures.addOnMoveListener(onMoveListener)
    }

    private fun initLocationComponent() {
        val locationComponentPlugin = mapView.location
        locationComponentPlugin.updateSettings {
            this.enabled = true
            this.locationPuck = LocationPuck2D(
                bearingImage = AppCompatResources.getDrawable(
                    this@MapBoxFragment.requireContext(),
                    R.drawable.mapbox_user_puck_icon,
                ),
                shadowImage = AppCompatResources.getDrawable(
                    this@MapBoxFragment.requireContext(),
                    R.drawable.mapbox_user_icon_shadow,
                ),
                scaleExpression = Expression.interpolate {
                    linear()
                    zoom()
                    stop {
                        literal(0.0)
                        literal(0.6)
                    }
                    stop {
                        literal(20.0)
                        literal(1.0)
                    }
                }.toJson()
            )
        }
        locationComponentPlugin.addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        locationComponentPlugin.addOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
        mapView.gestures.addOnMapLongClickListener(onLongClickListener)
    }

    private val onIndicatorBearingChangedListener = OnIndicatorBearingChangedListener {
        val newCamera = CameraOptions.Builder().bearing(it).pitch(cameraTilt).build()
        mapView.getMapboxMap().setCamera(newCamera)
    }

    private val onIndicatorPositionChangedListener = OnIndicatorPositionChangedListener {
        mapView.getMapboxMap().setCamera(CameraOptions.Builder().center(it).pitch(cameraTilt).build())
        mapView.gestures.focalPoint = mapView.getMapboxMap().pixelForCoordinate(it)
    }

    private val onLongClickListener = OnMapLongClickListener {
        onCameraTrackingResumed()
        resetMapZoom()
        true
    }

    private fun resetMapZoom() {
        mapView.getMapboxMap().setCamera(
            CameraOptions.Builder()
                .zoom(18.0).build())
    }

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            onCameraTrackingDismissed()
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {

        }
    }

    private fun onCameraTrackingResumed() {
        mapView.location
            .addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        mapView.location
            .addOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
        mapView.gestures.addOnMoveListener(onMoveListener)
    }

    private fun onCameraTrackingDismissed() {
        mapView.location
            .removeOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        mapView.location
            .removeOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
        mapView.gestures.removeOnMoveListener(onMoveListener)
    }
}