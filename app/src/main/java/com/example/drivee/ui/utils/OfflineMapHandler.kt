package com.example.drivee.ui.utils

import android.app.Activity
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import com.mapbox.bindgen.Value
import com.mapbox.common.*
import com.mapbox.geojson.Geometry
import com.mapbox.geojson.Polygon
import com.mapbox.maps.*
import android.util.Log
import android.widget.Toast
import android.widget.Toast.*
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.mapbox.maps.R
import com.mapbox.maps.extension.style.sources.generated.GeoJsonSource
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.example.drivee.MainActivity

import androidx.core.content.ContextCompat.getSystemService
import com.mapbox.geojson.Feature
import kotlin.math.roundToInt


class OfflineMapHandler(context: Context, activity: Activity) {
    val context = context
    val activity = activity
    val mapStyle = Style.MAPBOX_STREETS

    val tileStore = TileStore.create().also {
        // Set default access token for the created tile store instance
        it.setOption(
            TileStoreOptions.MAPBOX_ACCESS_TOKEN,
            TileDataDomain.MAPS,
            Value("sk.eyJ1IjoiZHJpdmVlIiwiYSI6ImNrd2J3bTIyMTA1aW0ybm4xeTl6ZDQzY3QifQ.qROo7mizkem1z54HMsj48A")
        )
    }

    val stylePackLoadOptions = StylePackLoadOptions.Builder()
        .glyphsRasterizationMode(GlyphsRasterizationMode.IDEOGRAPHS_RASTERIZED_LOCALLY)
        .metadata(Value("{ID: STYLE}"))
        .build()

    val offlineManager: OfflineManager =
        OfflineManager(MapInitOptions.getDefaultResourceOptions(context))

    val tilesetDescriptor = offlineManager.createTilesetDescriptor(
        TilesetDescriptorOptions.Builder()
            .styleURI(mapStyle)
            .minZoom(12)
            .maxZoom(18)
            .build()
    )

    fun downloadStylePack() {

        val stylePackCancelable = offlineManager.loadStylePack(
            mapStyle,
            // Build Style pack load options
            stylePackLoadOptions,
            { progress ->
                val comp = progress.completedResourceCount.toDouble()
                val req = progress.requiredResourceCount
                val fraction = comp / req
                val percent = (fraction * 100f).roundToInt()
                Log.d("MapDownload", "Downloading offline style pack ${percent}")
            },
            { expected ->
                activity.runOnUiThread {
                    makeText(
                        activity,
                        "Offline style pack downloaded successfully",
                        LENGTH_SHORT
                    ).show()
                }
                Log.d("MapDownload", "Offline style pack downloaded successfully")
                expected.error?.let {
                    activity.runOnUiThread {
                        makeText(
                            activity,
                            "Offline style pack download failed: ${it.message}",
                            LENGTH_SHORT
                        ).show()
                    }
                    Log.d("MapDownload", "Offline style pack download failed: $it")
                }
            }
        )
    }

    fun offlineMapAvailable(): Boolean {
        var isAvailable = false

        val tileRegionCancelable = tileStore.getAllTileRegions { expected ->
            if (expected.isValue) {
                expected.value?.let { tileRegionList ->
                    isAvailable = tileRegionList.size > 0
                }
            } else {
                expected.error?.let { tileRegionError ->
                    isAvailable = false
                }
            }
        }
        return isAvailable
    }

    fun downloadOfflineRegion() {
        downloadStylePack()
        val tileRegionCancelable = tileStore.loadTileRegion(
            "BP",
            tileRegionLoadOptions,
            { progress ->
                val comp = progress.completedResourceCount.toDouble()
                val req = progress.requiredResourceCount
                val fraction = comp / req
                val percent = (fraction * 100f).roundToInt()
                Log.d("MapDownload", "$percent")
            }
        ) { expected ->
            if (expected.isValue) {
                activity.runOnUiThread {
                    makeText(
                        activity,
                        "Offline region downloaded successfully",
                        LENGTH_SHORT
                    ).show()
                }
                Log.d("MapDownload", "Offline region downloaded successfully")
            }
            expected.error?.let {
                activity.runOnUiThread {
                    makeText(
                        activity,
                        "Offline region download failed: ${it.message}",
                        LENGTH_SHORT
                    ).show()
                }
                Log.d("MapDownload", "Offline region download failed: $it")
            }
        }
    }


    private val tileRegionLoadOptions = TileRegionLoadOptions.Builder()
        .geometry(getGeoJson())
        .descriptors(listOf(tilesetDescriptor))
        .metadata(Value("{ID: BP}"))
        .acceptExpired(true)
        .networkRestriction(NetworkRestriction.NONE)
        .build()

    private fun getGeoJson(): Geometry? {
        val polygonFeatureJson = """{
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              19.013385772705078,
              47.46500412484223
            ],
            [
              19.093379974365234,
              47.46500412484223
            ],
            [
              19.093379974365234,
              47.51778036836253
            ],
            [
              19.013385772705078,
              47.51778036836253
            ],
            [
              19.013385772705078,
              47.46500412484223
            ]
          ]
        ]
      }
    }"""
        val singleFeature: Feature = Feature.fromJson(polygonFeatureJson)
        return singleFeature.geometry() as Polygon
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

}