package com.example.drivee.ui.navigation

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.fragment.app.Fragment
import com.example.drivee.R
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.CameraPosition
import android.opengl.Visibility
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.FragmentManager
import com.example.drivee.MainActivity
import com.google.android.material.bottomappbar.BottomAppBar

import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton


class NavigationFragment : Fragment() {
    private val LOCATION_PERMISSION_REQUEST = 1
    private lateinit var locationRequest: LocationRequest
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var lastLocation: Location


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity)
        val supportMapFragment: SupportMapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        supportMapFragment.getMapAsync { googleMap ->
            handleCallback(googleMap, this.requireContext())
            createLocationRequest()
            startLocationUpdates(googleMap)
        }

        val navBar: BottomAppBar = requireActivity().findViewById(R.id.bottomAppBar)
        navBar.visibility = View.INVISIBLE

        val fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.visibility = View.INVISIBLE

        val stopButton: AppCompatButton = view.findViewById(R.id.btn_stop)
        stopButton.setOnClickListener {
            (requireActivity() as MainActivity).onNavigationStopped ()
        }

        return view
    }

    private fun handleCallback(map: GoogleMap, context: Context) {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                val locationList = locationResult.locations

                if (locationList.isNotEmpty()) {
                    lastLocation = locationList.last()
                    scrollToLocation(map, lastLocation)
                } else
                    Toast.makeText(context, "Waiting for GPS location...", Toast.LENGTH_SHORT)
                        .show()
            }
        }
    }

    private fun scrollToLocation(map: GoogleMap, location: Location) {
        val newLatLon = LatLng(location.latitude, location.longitude)

        val cameraPosition =
            CameraPosition.Builder().target(newLatLon).tilt(60f).zoom(20f).bearing(location.bearing)
                .build()

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    fun getLastLocation(): Location {
        return lastLocation
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates(map: GoogleMap) {
        //Check permissions
        if (!hasPermissions())
            requestPermissions(
                this.requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST
            )

        if (hasPermissions()) {
            //Kick off location updates
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
            //Display blue dot at location
            map.isMyLocationEnabled = true
        }
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create().apply {
            interval = 5000
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    private fun hasPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this.requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }


}