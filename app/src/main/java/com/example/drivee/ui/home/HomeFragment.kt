package com.example.drivee.ui.home

import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.drivee.R
import com.example.drivee.databinding.FragmentHomeBinding
import com.example.drivee.ui.map.TrafficSignAnalyzer
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mapbox.maps.extension.style.expressions.dsl.generated.number
import kotlinx.coroutines.*
import org.checkerframework.checker.signedness.SignednessUtil.toDouble
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.videoio.VideoCapture
import org.opencv.videoio.Videoio.CAP_PROP_POS_FRAMES
import org.tensorflow.lite.DataType
import org.tensorflow.lite.gpu.GpuDelegate
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.task.core.BaseOptions
import org.tensorflow.lite.task.vision.classifier.Classifications
import org.tensorflow.lite.task.vision.classifier.ImageClassifier
import org.tensorflow.lite.task.vision.detector.Detection
import org.tensorflow.lite.task.vision.detector.ObjectDetector
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.lang.Exception
import wseemann.media.FFmpegMediaMetadataRetriever




class HomeFragment : Fragment() {

    private lateinit var handler : Handler
    private lateinit var runnable : Runnable // reference to the runnable object
    private lateinit var trafficSignAnalyzer: TrafficSignAnalyzer

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private fun convertMatToBitMap(input: Mat): Bitmap? {
        var bmp: Bitmap? = null
        try {
            bmp = Bitmap.createBitmap(input.cols(), input.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(input, bmp)
        } catch (e: CvException) {
            Log.d("Exception", e.message!!)
        }
        return bmp
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val trafficSignHolder = binding.trafficSignHolder
        trafficSignAnalyzer = TrafficSignAnalyzer(this.context)

        val textView: TextView = binding.textHome
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        val navBar: BottomAppBar = requireActivity().findViewById(R.id.bottomAppBar)
        navBar.visibility = View.VISIBLE

        val fab: FloatingActionButton = requireActivity().findViewById(R.id.fab)
        fab.visibility = View.VISIBLE

        val inputStream: InputStream = this.resources.openRawResource(R.raw.drivee_test_vid)
        val outputFile = File (activity?.getDir(Environment.DIRECTORY_MOVIES, Context.MODE_PRIVATE), "drivee_test_vid.mp4")
        inputStream.use { input ->
            val outputStream = FileOutputStream(outputFile)
            outputStream.use { output ->
                val buffer = ByteArray(4 * 1024) // buffer size
                while (true) {
                    val byteCount = input.read(buffer)
                    if (byteCount < 0) break
                    output.write(buffer, 0, byteCount)
                }
                output.flush()
            }
        }
        val videoCap = VideoCapture()
        videoCap.open (outputFile.absolutePath)

        val imView = binding.imviewHome
        imView.visibility = View.VISIBLE

        val med = FFmpegMediaMetadataRetriever()
        med.setDataSource(outputFile.absolutePath)
        val duration: Long = med.metadata.getLong("duration")
        val frameRate: Double = med.metadata.getDouble("framerate")
        val numberOfFrame = (duration / frameRate).toInt()
        //var i = 0

/*        handler = Handler(Looper.getMainLooper())
        runnable = Runnable {
            if (++i == numberOfFrame/4) {
                handler.removeCallbacks(runnable)
            } else {
                Log.i ("FRAME", i.toString())
                val b: Bitmap = med.getFrameAtTime((i*1000*1000/frameRate*4).toLong(), FFmpegMediaMetadataRetriever.OPTION_CLOSEST)
                imView.setImageBitmap(b)
            }

            handler.post(runnable)
        }

        handler.post (runnable)*/

        var file = File ("/data/user/0/com.example.drivee/files/cropped0")
        if (file.exists()) {
            val bitmap = BitmapFactory.decodeStream(FileInputStream (file))
            imView.setImageBitmap(bitmap)
        }


        //videoCap.read (mat)
        //imView.setImageBitmap(convertMatToBitMap(mat))
        //while (videoCap.read (mat)) {

        //imView.setImageBitmap(convertMatToBitMap(mat))
        //}
        //videoCap.release ()

        //val bitmap = BitmapFactory.decodeStream(requireActivity().assets.open("drivee7.jpg"))

        //val gpuDelegate = GpuDelegate ()

        val options = ObjectDetector.ObjectDetectorOptions.builder()
            .setBaseOptions(BaseOptions.builder().useGpu().build())
            .setMaxResults(5)
            .setScoreThreshold(0.1f)
            .build()

        val detector = ObjectDetector.createFromFileAndOptions(requireActivity(), "regressor2.tflite", options)

        val options2 = ImageClassifier.ImageClassifierOptions.builder()
            .setBaseOptions(BaseOptions.builder().useNnapi().build())
            .setMaxResults(1)
            .setScoreThreshold(0.5f)
            .build()
        val imageClassifier = ImageClassifier.createFromFileAndOptions(
            requireActivity(), "narrow_classificator.tflite", options2
        )

        val btn = binding.playButton
        if (requireActivity().resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            btn.visibility = View.INVISIBLE
        } else {
            btn.visibility = View.VISIBLE
        }
        var pushed = false
        btn.setOnClickListener() {
            pushed = !pushed
            if (pushed) {
                btn.text = "STOP"
                var mat = Mat ()
                var i = 0
                var nextFrame : Boolean;
                handler = Handler(Looper.getMainLooper())
                runnable = Runnable {
                    ++i
                    if (i % 1 == 0) {
                        videoCap.set(CAP_PROP_POS_FRAMES, toDouble (i))
                        nextFrame = videoCap.read (mat)
                        if (!nextFrame) {
                            handler.removeCallbacks(runnable);
                            videoCap.release ()
                        } else {
                            val xt = 2.4286
                            val yt = 4.2857
                            val bitmapFrame = convertMatToBitMap(mat)
                            //imView.setImageBitmap(bitmapFrame)
                            //val rescaledImage = Bitmap.createScaledBitmap(bitmapFrame!!, 448, 448, false)
                            val detectionResults = runObjectDetection (bitmapFrame!!, detector)
                            if (detectionResults.isNotEmpty()) {
                                for (detection in detectionResults) {
                                    if (!bbThreshold (bitmapFrame, detection)) {
                                        continue
                                    }
                                    val croppedImage = cropImage(bitmapFrame, detection)
                                    val rescaledCroppedImage = Bitmap.createScaledBitmap(croppedImage, 224, 224, false)
                                    val classificationResults = runObjectClassification(rescaledCroppedImage, imageClassifier)
                                    if (classificationResults.isNotEmpty()) {
                                        val candidate = classificationResults[0]
                                        if (candidate.categories.isNotEmpty()) {
                                            //al trafficSignImView = trafficSignAnalyzer.chooseTrafficSignToDisplay(candidate.categories[0])
                                            //trafficSignHolder.addView(trafficSignImView)
                                            mat = drawBoundingBox(mat, detection, candidate.categories[0].label)
                                            //handler.postDelayed({trafficSignHolder.removeViewInLayout(trafficSignImView)}, 4000)
                                        }
                                        //Log.i ("CANDIDATE", candidate.categories.toString())
                                        Log.i ("CANDIDATE", "detected!")
                                    }
                                }
                            }
                            imView.setImageBitmap(convertMatToBitMap(mat))
                        }
                    } else {
                        videoCap.grab()
                    }

                    handler.post (runnable)
                }

                handler.post (runnable)
            } else {
                handler.removeCallbacks(runnable)
                btn.text = "PLAY"
            }
        }

        return root
    }

    private fun cropImage (bitmap: Bitmap, detection : Detection) : Bitmap {
        val h = bitmap.height
        val w = bitmap.width

        val bottom = if (detection.boundingBox.bottom < 0)  maxOf (0, detection.boundingBox.bottom.toInt()) else minOf (detection.boundingBox.bottom.toInt(), h)
        val top = if (detection.boundingBox.top < 0)  maxOf (0, detection.boundingBox.top.toInt()) else minOf (detection.boundingBox.top.toInt(), h)
        val left = if (detection.boundingBox.left < 0)  maxOf (0, detection.boundingBox.left.toInt()) else minOf (detection.boundingBox.left.toInt(), w)
        val right = if (detection.boundingBox.right < 0)  maxOf (0, detection.boundingBox.right.toInt()) else minOf (detection.boundingBox.right.toInt(), w)

        val y = top
        val x = left
        val height = (bottom) - y
        val width = (right) - x

        return Bitmap.createBitmap (bitmap, x, y, width, height)
    }

    private fun getLabelText(label: String) : String{
        val labels = listOf (
            "priority end",
            "one-way",
            "priority",
            "no entry",
            "parking",
            "forward or left",
            "yield",
            "danger",
            "left",
            "bus stop",
            "crosswalk",
            "30",
            "end 30"
        )

        return labels[label.toInt() - 1]
    }

    private fun drawBoundingBox(img: Mat, detection: Detection, label: String) : Mat {
        val h = img.rows()
        val w = img.cols()

        val bottom = if (detection.boundingBox.bottom < 0)  maxOf (0, detection.boundingBox.bottom.toInt()) else minOf (detection.boundingBox.bottom.toInt(), h)
        val top = if (detection.boundingBox.top < 0)  maxOf (0, detection.boundingBox.top.toInt()) else minOf (detection.boundingBox.top.toInt(), h)
        val left = if (detection.boundingBox.left < 0)  maxOf (0, detection.boundingBox.left.toInt()) else minOf (detection.boundingBox.left.toInt(), w)
        val right = if (detection.boundingBox.right < 0)  maxOf (0, detection.boundingBox.right.toInt()) else minOf (detection.boundingBox.right.toInt(), w)

        val y = top
        val x = left
        val height = (bottom) - y
        val width = (right) - x

        Imgproc.rectangle(img, Rect (x, y, width, height), Scalar (255.0, 0.0, 0.0), 5)
        Imgproc.putText(img,
            getLabelText (label),
            Point((x+width+50).toDouble(), (y+height/2).toDouble()),
            3,
            2.0,
            Scalar (255.0, 0.0, 0.0),
            2)

        return img
    }

    private fun bbThreshold (bitmap: Bitmap, detection: Detection) : Boolean {
        val h = bitmap.height
        val w = bitmap.width

        val bottom = if (detection.boundingBox.bottom < 0)  maxOf (0, detection.boundingBox.bottom.toInt()) else minOf (detection.boundingBox.bottom.toInt(), h)
        val top = if (detection.boundingBox.top < 0)  maxOf (0, detection.boundingBox.top.toInt()) else minOf (detection.boundingBox.top.toInt(), h)
        val left = if (detection.boundingBox.left < 0)  maxOf (0, detection.boundingBox.left.toInt()) else minOf (detection.boundingBox.left.toInt(), w)
        val right = if (detection.boundingBox.right < 0)  maxOf (0, detection.boundingBox.right.toInt()) else minOf (detection.boundingBox.right.toInt(), w)

        val y = top
        val x = left
        val height = (bottom) - y
        val width = (right) - x

        return height < 200 && width < 200
    }

    private fun runObjectDetection(bitmap: Bitmap, detector: ObjectDetector): List<Detection> {
        val image = TensorImage.fromBitmap(bitmap)

        return detector.detect(image)
    }

    private fun runObjectClassification (bitmap: Bitmap, imageClassifier: ImageClassifier) : List<Classifications> {
        val tensorImage = TensorImage(DataType.UINT8)
        tensorImage.load(bitmap);

        val results = imageClassifier.classify(tensorImage)
        return results
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}